import Amps from './classes/Amps';

const init = async () => {
  const amps = new Amps();

  await amps.init();

  const ampsClient = amps.getAmpsClient();
  const connected = amps.getClientConnectionStatus();

  if (connected) {
    ampsClient.onConnect();
    // ampsClient.publish();
  } else {
    console.log('not subscribed');
  }
};

init();

import * as amps from 'amps';

const init = async () => {
  const client = new amps.Client('my-application');

  const conn = await client
    .connect('ws://localhost:9008/amps/json')
    .catch(error =>
      // This can be either a connection error
      // or a subscription error, thanks to Promises!
      console.log(error)
    );

  console.log('conn => ', client.isConnected(), conn);

  await client.subscribe(message => {
    console.log(message.data);
  }, 'customers');

  await client.publish('customers', {
    id: '2',
    name: 'lalala',
  });
};

init();

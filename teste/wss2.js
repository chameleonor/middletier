import * as amps from 'amps';

let reconnect = null;

// Assign an error handler that will handle general error
// such as disconnects
const client = new amps.Client('my-app').errorHandler(function(err) {
  console.log(err, 'Reconnecting after 5 seconds...');
  setTimeout(reconnect, 5000);
});

function onConnect() {
  return client
    .subscribe(function(message) {
      console.log('message: ', message);
    }, 'customers')
    .catch(function(err) {
      console.log('Subscription error: ', err);
    });
}

reconnect = function() {
  return client
    .connect('wss://localhost:9008/amps/json')
    .then(onConnect)
    .catch(function(err) {
      // Connection error occurred
      console.log('err => ', err);
      setTimeout(reconnect, 5000);
    });
};

// Begin by connecting and subscribing
reconnect(onConnect);

import AmpsClient from './AmpsClient';

class Amps {
  constructor() {
    const ampsClientConfig = {
      name: 'teste',
      errorHandler: (err, callback) => {
        console.log('callback ===> ', callback);
        // callback();
      },
      uris: ['ws://localhost:9008/amps/json', 'ws://localhost:9010/amps/json'],
    };

    this.ampsClient = new AmpsClient(ampsClientConfig);
  }

  init = async () => {
    await this.ampsClient.start();
    console.log('class amps init client started!');
  };

  onConnect = () => this.ampsClient.onConnect();

  getAmpsClient = () => this.ampsClient;

  publish = () => this.ampsClient.publish();

  getClientConnectionStatus = () => this.ampsClient.connection;
}

export default Amps;

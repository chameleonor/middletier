import * as amps from 'amps';
import { throws } from 'assert';

class AmpsClient {
  constructor(config) {
    this.config = config;

    this.client = new amps.Client(this.config.name);
    this.serverChooser = new amps.DefaultServerChooser();
    // const strategy = new amps.ExponentialDelayStrategy({
    //   initialDelay: 400,
    //   maximumDelay: 5000,
    //   backoffExponent: 1.5,
    //   maximumRetryTime: 0,
    //   jitter: 1,
    // });

    this.currentUri = 0;

    // this.client.errorHandler(this.errorHandler);
    this.client.disconnectHandler(this.disconnectHandler);
    this.client.reconnectDelay(3000);
    // this.client.delayStrategy(strategy);
    this.client.heartbeat(5);
    this.client.bookmarkStore(new amps.MemoryBookmarkStore());
    this.setServerChooser();

    this.currentUri = null;

    return this;
  }

  nextUriServer = async () => {
    console.log('Switching to the next URI...');
    this.currentUri = (this.currentUri + 1) % this.config.uris.length;

    console.log('this.currentUri => ', this.currentUri);

    await new Promise(resolve => {
      setTimeout(resolve, 4000);
    });

    this.connectToNextUri(this.config.uris[this.currentUri]);
  };

  // disconnectHandler = async (client, err) => {
  //   console.log('disconnectHandler ==> ', err);

  //   if (err.name === '1006') {
  //     this.nextUriServer();
  //   }
  // };

  disconnectHandler = async (client, err) => {
    console.log('disconnectHandler ==> ', err);

    if (this.currentUri !== this.serverChooser.getCurrentURI()) {
      this.start();
    }
  };

  errorHandler = err => {
    console.error(`errorHandler => Error => `, err);
    // return this.config.errorHandler(err, this.start);
  };

  setServerChooser = () => {
    const { uris } = this.config;
    for (let x = 0; x < uris.length; x += 1) {
      this.serverChooser.add(uris[x]);
    }

    this.client.serverChooser(this.serverChooser);
  };

  // start = () => this.connectToNextUri(this.config.uris[0]);

  start = async () => {
    console.log(
      '\n\nthis.serverChooser ====> ',
      this.serverChooser.getCurrentURI()
    );

    this.currentUri = this.serverChooser.getCurrentURI();

    try {
      const connection = await this.client.connect();
      console.log('connection =====> ', connection);
      if (connection) {
        this.onConnect();
      }
    } catch (error) {
      console.log('catch connect => ', error, '\n\n');
    }
  };

  connectToNextUri = async uri => {
    console.log(
      '\n\nthis.serverChooser ====> ',
      // this.serverChooser.getCurrentURI()
      uri
    );

    try {
      const connection = await this.client.connect(uri);
      console.log('connection =====> ', connection);
      if (connection) {
        this.onConnect();
      }
    } catch (error) {
      console.log('catch connect => ', error, '\n\n');
    }
  };

  onConnect = () => {
    if (this.connection) {
      console.log('this.connection ===> ', this.client.isConnected());
      this.subscribe();
    }
  };

  subscribe = async () => {
    try {
      return await this.client.subscribe(message => {
        console.log(message.data);
      }, 'customers');
    } catch (error) {
      console.log('Subscription error: ', error);
    }
  };

  publish = async () => {
    try {
      this.client.publish('customers', {
        id: '28391',
        name: 'lululu',
      });
    } catch (error) {
      console.log('Publish error: ', error);
    }
  };

  checkAndSendHeartbeat = () => {
    this.client.checkAndSendHeartbeat();
  };

  isConnected = () => this.client.isConnected();

  disconnect = () => {
    this.client.disconnect();
  };
}

export default AmpsClient;
